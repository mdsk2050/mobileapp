CREATE DATABASE  IF NOT EXISTS `mobileapppoc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mobileapppoc`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: LOCALHOST    Database: mobileapppoc
-- ------------------------------------------------------
-- Server version	5.6.39-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aboutpark`
--

DROP TABLE IF EXISTS `aboutpark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aboutpark` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `image` text,
  `about` text,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aboutpark`
--

LOCK TABLES `aboutpark` WRITE;
/*!40000 ALTER TABLE `aboutpark` DISABLE KEYS */;
INSERT INTO `aboutpark` VALUES (1,'Dress Code ','Men & Women','https://leslie.salkeiz.k12.or.us/wp-content/uploads/sites/6/2018/05/Dress-code.jpeg','Please, don\'t wear a fanny pack. Inside your bag remember to pack sunglasses, sunscreen, hand sanitizer, baby wipes and, if you are wearing a white top, a dark tank to ride the water rides in. Some people pack bathing suits, but I\'m not going to walk around any part of any theme park in my bathing suit.','A'),(2,'Park Timings','All Days','https://brrm.com/wp-content/uploads/2014/04/stopwatch.png','Normal Season Weekdays: 11:00 AM to 6:00 PM Normal Season Weekends/Holidays: 11:00 AM to 7:00 PM Peak Season: 11:00 AM to 7:00 PM Water Park Timings on Normal Season Weekdays : 12:30 PM to 5:00 PM Water Park Timings on Peak Season and Normal Season Weekends/Holidays : 12:00 PM to 6:00 PM','A'),(3,'Ticket Prices','Normal Track','http://d2dzjyo4yc2sta.cloudfront.net/?url=images.pitchero.com%2Fui%2F472185%2Fimage_5b320c00073b5.png&w=820&t=fit&q=85','Normal Season (Weekdays) ** Adult Regular : Rs. 770/- +18% GST ** Child* Regular : 615/- +18% GST ** Senior Citizen (60-69 years): 580 + 18% GST ** Senior Citizen (70 years & above): 385 + 18% GST ** Defence: Rs. 615/- +18% GST ** Fastrack Adult : 1155/- + 18% GST ** Fastrack Child* : 925/- + 18% GST','A'),(4,'Ticket Prices','Fast Track','https://www.brandcrowd.com/gallery/brands/pictures/picture14297419539985.png','Normal Season (Weekdays) ** Adult : 1155/- + 18% GST ** Child* : 925/- + 18% GST ** Normal Season (Weekdays) ** Adult : 1780/- + 18% GST ** Child* : 1440/- + 18% GST','A'),(5,'Cancellation Policy','For All Issues','https://st3.depositphotos.com/1186248/13449/i/1600/depositphotos_134494002-stock-photo-cancellation-policy-rubber-stamp.jpg','Same day cancellations or No Show- No Refund 0-48 hours (from midnight of the check-in date)- 50% refund 3) Above 48 hours (from midnight of the check-in date)- 75% Refund 4) All refunds would be credited to the user’s payment mode within 7-14 bank working days.5) Partial cancellations are not permitted','A'),(6,'Facilities','All','https://my.northland.edu/wp-content/uploads/2018/02/Maintenance-directory.jpg','Lockers & Dress Changing Rooms: To ensure safety of your belongings, safety lockers are made available at the changing room, so that the visiting guests can load their valuables safely. The park is also equipped with separate dress changing rooms for gents and ladies inside the park.Drinking Water: Safe and hygienic drinking water is available near prominent attractions in the park.First Aid: First Aid facility is available at the park in case of emergencies.ATM Counter: Cash Dispensing Machines are available at the parking and entrance areas of the park.Prayer Room: Wonderla Park provides a prayer room facility for the convenience of our visitors. Check with guest relation staff for assistance.Cloak Room:: Wonderla has a cloak room which can be used to store heavier baggage. Please contact guest relations or entry personnel for assistance. Extra charges apply.Parking: Wonderla has abundant parking space for the visitors to park their vehicles','A'),(7,'Contact Us','Any Time','http://mediad.publicbroadcasting.net/p/klcc/files/styles/x_large/public/201610/contact-us.jpg','For Any Queries and Conserns Contact Us : 99099 99099  Email: contact@themepark.com Phone: +91-484-2684001 - 2684007 Enquires: +91 75 9385 3107 enquiry@themepark.com','A');
/*!40000 ALTER TABLE `aboutpark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerdata`
--

DROP TABLE IF EXISTS `customerdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `ticketnumber` text,
  `image` text,
  `tickettype` varchar(45) DEFAULT NULL,
  `coordinates` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerdata`
--

LOCK TABLES `customerdata` WRITE;
/*!40000 ALTER TABLE `customerdata` DISABLE KEYS */;
INSERT INTO `customerdata` VALUES (1,'Dsk',18,'A','0','0.49588748068705457','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png',NULL,NULL),(2,'Dsk',18,'A','0','0.30203323052582687','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png',NULL,NULL),(3,'Kgf',18,'A','1','0.6636542528486304','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png',NULL,NULL),(4,'Kgf',18,'A','0','0.8685924409699977','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Fast Track',NULL),(5,'Hi',18,'A','0','0.6889509620742909','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(6,'Hi',18,'A','1','0.9565081269710831','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Regular',NULL),(7,'Dsk',19,'A','0','0.737324545572501','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular ',NULL),(8,NULL,19,'A',NULL,'0.619916617700599','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png',NULL,NULL),(9,'Hi',13,'A','0','0.36313908313802723','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(10,'testing',13,'A','0','0.2794419102030159','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(11,'Testing',13,'A','0','0.40274729375081875','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(12,'Testing',13,'A','0','0.2302732462148691','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(13,'Testing',13,'A','0','0.09598002518398197','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(14,'Testing',13,'A','0','0.5481738408031887','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(15,'Testing',13,'A','0','0.8610728505007104','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(16,'Testing',13,'A','0','0.4600118500979007','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(17,'Testing',13,'A','0','0.1276232620349136','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(18,'Testing',13,'A','0','0.9889619872047781','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Fast Track',NULL),(19,'Testing',13,'A','0','0.016161908524702673','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Fast Track',NULL),(20,'Testing',13,'A','0','0.5192527193707837','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Fast Track',NULL),(21,'Testing',13,'A','0','0.2396058180912477','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(22,'Testing',13,'A','0','0.9667643872376914','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(23,'Testing',13,'A','0','0.8142510041008175','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(24,'Testing',13,'A','0','0.3276503718881838','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(25,'Testing',13,'A','0','0.06176359073107851','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(26,'Testing',13,'A','0','0.8245816290096231','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(27,'Testing',13,'A','0','0.8607903606728624','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(28,'Testing',13,'A','0','0.23613227445375662','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(29,'Testing',13,'A','0','0.246080351760966','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(30,'Testing',13,'A','0','0.1212304642923181','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(31,'Testing',13,'A','0','0.3360859524956925','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(32,'Testing',13,'A','0','0.3115265271545642','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(33,'Testing',13,'A','0','0.6641903454315341','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(34,'Testing',13,'A','0','0.10267022339738952','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(35,'Testing',13,'A','0','0.23821618636325614','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(36,'Testing',13,'A','0','0.8798938698656933','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(37,'Testing',13,'A','0','0.5065621598567847','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(38,'Testing',13,'A','0','0.13681405719367534','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(39,'Testing',13,'A','0','0.7515387062848307','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(40,'Testing',13,'A','0','0.7106506700683226','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(41,'Testing',13,'A','0','0.7607581352402988','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(42,'Testing',13,'A','0','0.773524016150791','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(43,'Ttt',13,'A','0','0.22308936316001482','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(44,'Testing',13,'A','0','0.7851301631580916','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(45,'Testing',13,'A','0','0.40508957872140217','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(46,'Testing',13,'A','0','0.6958586102980271','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(47,'Testing',13,'A','0','0.4026155614807163','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(48,'Testing',13,'A','0','0.34344537378436146','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(49,'Testing',13,'A','0','0.799962384375356','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(50,'Testing',13,'A','0','0.39734339062956625','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(51,'Testing',13,'A','0','0.1967766696666322','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(52,'Testing',13,'A','0','0.6134615584683456','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(53,'Testing',13,'A','0','0.4730915477673059','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(54,'Testing',13,'A','0','0.7688356814118493','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(55,'Testing',13,'A','0','0.6323050474965184','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(56,'Testing',13,'A','0','0.267751973906587','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(57,'Testing',13,'A','0','0.568547077666298','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(58,'Testing',13,'A','0','0.5377372213102629','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Regular',NULL),(59,'Testing',13,'A','0','0.3964497604855972','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Regular',NULL),(60,'Yesterday',14,'A','0','0.32879191810920916','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(61,'Yesterday',14,'A','0','0.05729336789889938','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(62,'Reshma',14,'A','1','0.49005970102931196','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL),(63,'Tt',14,'A','0','0.15050986576487713','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(64,'Tt',14,'A','0','0.311574475381009','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(65,'Testings',14,'A','0','0.1646831023521853','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(66,'Testing',14,'A','0','0.009671095776334182','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(67,'Thempark',14,'A','0','0.9521102779465247','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(68,'Testing',14,'A','0','0.7353583233833001','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(69,'Testing',14,'A','0','0.6238723207667451','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(70,'Testing',14,'A','0','0.04243148301462285','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Fast Track',NULL),(71,'Yesterday',14,'A','0','0.7154675393761247','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(72,'Lavanya',17,'A','1','0.10923819996827877','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(73,'DHARANI SAI KUMAR',17,'A','0','0.8272599494105466','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Fast Track',NULL),(74,'Sai',17,'A','0','0.10178916275987149','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Adult Regular',NULL),(75,'Ryan',12,'A','0','0.4547852598702844','https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png','Child* Fast Track',NULL);
/*!40000 ALTER TABLE `customerdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `likes` bigint(20) DEFAULT NULL,
  `image` text,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (1,100,'https://res.cloudinary.com/simplotel/image/upload/w_5000,h_3333/x_0,y_0,w_5000,h_2812,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/_60A0879_stc1on','A'),(2,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_70,w_1400,h_789,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD1kids-5912_cbdcdj','A'),(3,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_131,w_1400,h_788,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/Wonderla_COC_Day1-4378_h8n78a','A'),(4,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_176,w_732,h_1029,r_0,c_crop,q_60,fl_progressive/w_1100,f_auto,c_fit/wonderla-amusement-parks-resort/Dress_code_leaflet-Actual2_bhrxgy','A'),(5,100,'https://res.cloudinary.com/simplotel/image/upload/w_5000,h_3333/x_0,y_1216,w_5000,h_1261,r_0,c_crop,q_60,fl_progressive/w_1100,f_auto,c_fit/wonderla-amusement-parks-resort/Wonderla_Amazement_Park_and_Resort_Bangalore_Kochi_and_Hyderabad_23_lpy6vn','A'),(6,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_404,w_1964,h_1104,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-park-bangalore/water_rides_boomerang_1_wonderla_amusement_parks_bangalore_ub1t5e','A'),(7,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_130,w_1400,h_789,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WCDay2-5093_gsd13p','A'),(8,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_97,w_1417,h_798,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/RECOIL_venkwi','A'),(9,100,'https://res.cloudinary.com/simplotel/image/upload/x_123,y_216,w_1033,h_581,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD5-13.3.16-7338_gcvrs1','A'),(10,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_1,w_1500,h_843,r_0,c_crop,q_60,fl_progressive/w_1100,f_auto,c_fit/wonderla-amusement-parks-resort/Fatrack-tickets-01_zuml6r','A'),(11,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_119,w_1400,h_788,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD1kids-5856_xu9yjy','A'),(12,100,'https://res.cloudinary.com/simplotel/image/upload/x_0,y_223,w_1200,h_676,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD2-10.3.16-6424_kzbpoh','A'),(13,100,'https://res.cloudinary.com/simplotel/image/upload/x_119,y_244,w_1139,h_641,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD1kids-5706_aauj8v','A'),(14,100,'https://res.cloudinary.com/simplotel/image/upload/x_427,y_278,w_958,h_539,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD1kids-5830_ein8mu','A'),(15,100,'https://res.cloudinary.com/simplotel/image/upload/x_63,y_397,w_1691,h_951,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/WONDER_SPLASH_ir9ja9','A'),(16,100,'https://res.cloudinary.com/simplotel/image/upload/x_136,y_127,w_958,h_539,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/hyderabad-park/WBD4set2-12.3.16-7143_gvgfps','A'),(17,NULL,NULL,'');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `content` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` text,
  `time` varchar(100) DEFAULT NULL,
  `description` text,
  `image` text,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (1,'Early Bird Offer - 10% off','Valid up to DEC 31 2019','Plan your visit 10 days in advance and get a flat 10% off on entry tickets','https://www.adlabsimagica.com/wp-content/uploads/banner_earlybird_2.jpg','A'),(2,'Bulk Booking Online- 5% Off','Valid up to DEC 31 2019','Now book your tickets to all  Amusement Parks online and create magical memories with your loved one, without any wait. What\'s more is that you get a discount of flat 5% every time you book more than 6 tickets* online','https://res.cloudinary.com/simplotel/image/upload/x_0,y_7,w_411,h_231,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/corporate_groups_bbhrwo','A'),(3,'College ID Offer- 20% off','Valid up to DEC 31 2019','We love students - College students can avail exciting discounted ticket prices on entry tickets to Amusement Parks','https://res.cloudinary.com/simplotel/image/upload/x_0,y_0,w_1200,h_675,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/Wonderla_Amusement_Park_Hall_Ticket_Offer_d3ux8k','A'),(4,'All Day Meal Package at 299/-','Valid up to DEC 31 2019','Book your meal package and get three scrumptious meals at just 299/-','https://res.cloudinary.com/simplotel/image/upload/x_17,y_0,w_404,h_227,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/full_day_meal_package_vykrcg','A'),(5,'25% Off On Winter','Valid up to DEC 31 2019','Plan your visit  in Winter and get a flat 10% off on entry tickets','https://www.woodlandspark.com/wp-content/uploads/2016/11/Winter-special-offer-title.png','A');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `price` text,
  `image` text,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (1,'Adult Regular','Rs. 1000/- +18% GST','https://thumbs.gfycat.com/SevereSparseGoitered-small.gif','A'),(2,'Adult Fast Track','Rs. 1500/- +18% GST','https://png.pngtree.com/element_origin_min_pic/16/12/14/5aaa4619aaf0fa6feb3cb7af67c21310.jpg','A'),(3,'Child* Regular','Price Rs. 650 + 18% GST','https://d33wubrfki0l68.cloudfront.net/1431452fd2a26afa7e480b1a5256e745649a175e/3e017/static/images/blog/2016-10-17-wonderful-surprises/goldenticket.gif','A'),(4,'Child* Fast Track','Price Rs. 1000 + 18% GST','http://yumarodeo.com/wp-content/uploads/2012/11/vip-tickets.png','A');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mobileapppoc'
--

--
-- Dumping routines for database 'mobileapppoc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-31 15:30:07
