
let app = require('../server/server')
const { Expo } = require('expo-server-sdk')
loopback = app.dataSources.mobileapppoc.connector
  /**
   * @author Hemanth Sai Ram
   * customer app service
   */

function customerService(){


this.saveCustomerData= function(obj,callback){

  var d = new Date();
  // obj.time = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()
  obj.time = d.getHours()
  obj.status = 'A'
  obj.ticketnumber = Math.random()
  obj.image = 'https://socialdiets.files.wordpress.com/2012/06/cityville-roller_coaster_mega_buildablel.png'
  console.log('hi ', JSON.stringify(obj))
  app.models.Customerdata.create(obj).then((cusDataSaveRes)=>{
    console.log(cusDataSaveRes)
      let expo = new Expo();
      let messages = [];
      // let pushToken =  {"token":"ExponentPushToken[GA8xbwL0dPAdYSXIGkB1PF]"}
    let pushToken =  {"token":"ExponentPushToken[mkTlD7AfmQ-zM1mNUj7S2K]"}
// DSK let pushToken =  {"token":"ExponentPushToken[e_0cSXINaI-1Zmgjk9PZUy]"}
      messages.push({
        to: pushToken.token,
        sound: 'default',
        body: obj.name+' '+ 'has just bought the, Ticket No: '+obj.ticketnumber,
        data: { withSome: obj },
      })
      expo.sendPushNotificationsAsync(messages)
      callback(null,cusDataSaveRes)
  })
  }

  this.getCustomerData = function(obj,callback){
    let data = [
      {
        seriesName: 'male',
        data:[],
        color:'blue'
      },
      {
        seriesName: 'female',
        data:[],
        color:'pink'
      }
    ]
    let maleQuery1 = 'select count(*) as count from customerdata cd where cd.gender = "0" and cd.status = "A" and cd.time  = 13'
    let maleQuery2 = 'select count(*) as count from customerdata cd where cd.gender = "0" and cd.status = "A" and cd.time  = 14'
    // let maleQuery3 = 'select count(*) as count from customerdata cd where cd.gender = "0" and cd.status = "A" and cd.time  = 01'
    let femmaleQuery1 = 'select count(*) as count  from customerdata cd where cd.gender = "1" and cd.status = "A" and cd.time  = 13'
    let femmaleQuery2 = 'select count(*) as count  from customerdata cd where cd.gender = "1" and cd.status = "A" and cd.time  = 14'
    // let femmaleQuery3 = 'select count(*) as count  from customerdata cd where cd.gender = "1" and cd.status = "A" and cd.time  = 01'
    loopback.query(maleQuery1,(err,maleRes1)=>{
      data[0].data.push({x:'01',y:maleRes1[0].count})
      loopback.query(maleQuery2,(err,maleRes2)=>{
        data[0].data.push({x:'02',y:maleRes2[0].count})
        // loopback.query(maleQuery3,(err,maleRes3)=>{
          // data[0].data.push({x:'01',y:maleRes3[0].count})
          loopback.query(femmaleQuery1,(err,femaleres1)=>{
            data[1].data.push({x:'01',y:femaleres1[0].count})
            loopback.query(femmaleQuery2,(err,femaleres2)=>{
              data[1].data.push({x:'02',y:femaleres2[0].count})
              // loopback.query(femmaleQuery3,(err,femaleres3)=>{
                // data[1].data.push({x:'01',y:femaleres3[0].count})
                callback(null,data)
              // })
            })

          })
        })
      // })
    })
  }
 /**
  * @author:- DHARANI SAI
  */

  this.aboutpark=function(aboutparkData,callback){
    query="SELECT * FROM aboutpark ap WHERE ap.status ='A'"
    loopback.query(query,aboutparkData,function (err, AboutParkObj) {
      console.log(AboutParkObj);
              callback(null, AboutParkObj)
          })
  }
   /**
  * @author:- DHARANI SAI
  */

 this.gallery=function(galleryData,callback){
  query="SELECT * FROM gallery g WHERE g.status='A'"
  loopback.query(query,galleryData,function (err, GalleryObj) {
    console.log(GalleryObj);
            callback(null, GalleryObj)
        })
}

   /**
  * @author:- DHARANI SAI
  */

 this.parkoffers=function(offersData,callback){
  query="SELECT * FROM offers o WHERE o.status='A'"
  loopback.query(query,offersData,function (err, offersObj) {
    console.log(offersObj);
            callback(null, offersObj)
        })
}

   /**
  * @author:- DHARANI SAI
  */

 this.parkTickets=function(parkTicketsData,callback){
  query="SELECT * FROM tickets t WHERE t.status='A'"
  loopback.query(query,parkTicketsData,function (err, parkTicketsObj) {
    console.log(parkTicketsObj);
            callback(null, parkTicketsObj)
        })
}


/**
 * @author:- DHARANI SAI
 */
this.GenearateOffers= function(obj,callback){

  // var d = new Date();
  // obj.time = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()
  obj.time = 'Valid up to DEC 31 2019'
  obj.status = 'A'
  obj.description=' Hurry up Limited Period Offer '
  obj.image = 'http://wayanad.keralaportal.in/wp-content/uploads/sites/3/2019/01/special-offer.gif'
  console.log('hi ', JSON.stringify(obj))
  app.models.Offers.create(obj).then((GenerateOffersRes)=>{
    console.log(GenerateOffersRes)
      let expo = new Expo();
      let messages = [];
      // let pushToken =  {"token":"ExponentPushToken[GA8xbwL0dPAdYSXIGkB1PF]"}
      //HEMA let pushToken =  {"token":"ExponentPushToken[mkTlD7AfmQ-zM1mNUj7S2K]"}
let pushToken =  {"token":"ExponentPushToken[e_0cSXINaI-1Zmgjk9PZUy]"}
      messages.push({
        to: pushToken.token,
        sound: 'default',
        body: 'Best Offers '+' Get up to '+ obj.title +' '+ obj.time +'   '+ obj.description,
        data: { generateoffers: obj },
      })
      expo.sendPushNotificationsAsync(messages)
      callback(null,GenerateOffersRes)
  })
  }


}

customerService = new customerService()

module.exports = customerService
