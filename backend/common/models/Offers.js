'use strict';module.exports=function(Offers) {
    let customerService = require('../../customerappservices/customerService');

    Offers.parkoffers = function(offersData, callback) {
        customerService.parkoffers(offersData, function(err, result) {
            callback(null, result);
        });
    };

    Offers.remoteMethod('parkoffers', {
        accepts: { arg: 'offersData', type: 'array',http:{source:'body'} },
        returns: { arg: 'offersData', type: 'object', root: true },
        http: {
            path: '/parkoffers',
            verb: 'get'
        }
    });

    
    Offers.GenearateOffers = function(Obj, callback) {
        console.log('---',Obj)
        customerService.GenearateOffers(Obj, function(err, result) {
            callback(null, result);
        });
    };

    Offers.remoteMethod('GenearateOffers', {
        accepts: { arg: 'Obj', type: 'object',http:{source:'body'} },
        returns: { arg: 'Obj', type: 'object', root: true },
        http: {
            path: '/GenearateOffers',
            verb: 'post'
        }
    });
};