'use strict';module.exports=function(Aboutpark) {

let customerService = require('../../customerappservices/customerService');

Aboutpark.aboutpark = function(aboutparkData, callback) {
        customerService.aboutpark(aboutparkData, function(err, result) {
            callback(null, result);
        });
    };

    Aboutpark.remoteMethod('aboutpark', {
        accepts: { arg: 'aboutparkData', type: 'array',http:{source:'body'} },
        returns: { arg: 'aboutparkData', type: 'object', root: true },
        http: {
            path: '/aboutpark',
            verb: 'get'
        }
    });
};