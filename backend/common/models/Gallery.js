'use strict';module.exports=function(Gallery) {
    let customerService = require('../../customerappservices/customerService');

    Gallery.gallery = function(galleryData, callback) {
        customerService.gallery(galleryData, function(err, result) {
            callback(null, result);
        });
    };

    Gallery.remoteMethod('gallery', {
        accepts: { arg: 'galleryData', type: 'array',http:{source:'body'} },
        returns: { arg: 'galleryData', type: 'object', root: true },
        http: {
            path: '/gallery',
            verb: 'get'
        }
    });
};