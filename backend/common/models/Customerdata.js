'use strict';module.exports=function(Customerdata) {

  let customerService = require('../../customerappservices/customerService');

      Customerdata.saveCustomerData = function(Obj, callback) {
        console.log('---',Obj)
        customerService.saveCustomerData(Obj, function(err, result) {
            callback(null, result);
        });
    };

        Customerdata.remoteMethod('saveCustomerData', {
        accepts: { arg: 'Obj', type: 'object',http:{source:'body'} },
        returns: { arg: 'Obj', type: 'object', root: true },
        http: {
            path: '/saveCustomerData',
            verb: 'post'
        }
    });
      Customerdata.getCustomerData = function(Obj, callback) {
        console.log('--->',Obj)
        customerService.getCustomerData(Obj, function(err, result) {
            callback(null, result);
        });
    };

        Customerdata.remoteMethod('getCustomerData', {
        accepts: { arg: 'Obj', type: 'object' },
        returns: { arg: 'Obj', type: 'object', root: true },
        http: {
            path: '/getCustomerData',
            verb: 'get'
        }
    });

};
