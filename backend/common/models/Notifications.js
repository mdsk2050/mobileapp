'use strict';
module.exports=function(Notifications) {

  let notificationService = require('../../server/pushnotifications/pushNotificationService');


  Notifications.pushNotificationToken = function(Obj, callback) {
    console.log('---',Obj)
    notificationService.pushNotificationToken(Obj, function(err, result) {
        callback(null, result);
    });
};
Notifications.remoteMethod('pushNotificationToken', {
    accepts: { arg: 'Obj', type: 'object',http:{source:'body'} },
    returns: { arg: 'Obj', type: 'object', root: true },
    http: {
        path: '/pushNotificationToken',
        verb: 'post'
    }
});
};
