'use strict';module.exports=function(Tickets) {
    let customerService = require('../../customerappservices/customerService');

    Tickets.parkTickets = function(parkTicketsData, callback) {
        customerService.parkTickets(parkTicketsData, function(err, result) {
            callback(null, result);
        });
    };

    Tickets.remoteMethod('parkTickets', {
        accepts: { arg: 'parkTicketsData', type: 'array',http:{source:'body'} },
        returns: { arg: 'parkTicketsData', type: 'object', root: true },
        http: {
            path: '/parkTickets',
            verb: 'get'
        }
    });
};