import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator,createAppNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import QRcode from '../screens/QRcode';
import Blog from  '../screens/dsk';
import Users from '../screens/info';
import Galleries from '../screens/images';
import quizz from '../screens/appp';
import tickets from '../screens/tickets';
import Latestoffers from '../screens/latestoffers';
import StartingScreen from '../screens/startingpage';
import SignUp from '../screens/signup';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Buytickets: StartingScreen,
  Qr: QRcode,
 
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const images = createStackNavigator({
  Info: Galleries,
});

images.navigationOptions = {
  tabBarLabel: 'Gallery',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}

const DskStack = createStackNavigator({
  Offers: Blog,
});

DskStack.navigationOptions = {
  tabBarLabel: 'Offers',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const DskStack1 = createStackNavigator({
  Info: Users,
});

DskStack1.navigationOptions = {
  tabBarLabel: 'About Park',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}

const Trivia = createStackNavigator({
  Info: quizz,

});

Trivia.navigationOptions = {
  tabBarLabel: 'Trivia',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}

// const Qr = createStackNavigator({
//   Qrs: QRcode,
// });

// Qr.navigationOptions = {
//   tabBarLabel: 'Qr',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={Platform.OS === 'ios' ? 'ios-qr-scanner' : 'md-qr-scanner'}
//     />
//   ),
// };
const tkt = createStackNavigator({
  Info: tickets,
});

tkt.navigationOptions = {
  tabBarLabel: 'Buy ticket',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}

const newoffers = createStackNavigator({
  Info: Latestoffers,
});

newoffers.navigationOptions = {
  tabBarLabel: 'Buy ticket',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}

const startingStack = createStackNavigator({
  Buytickets: StartingScreen,
  Qr: QRcode,
  Tkt:tickets,
  LatestOffers:Latestoffers,

});

startingStack.navigationOptions = {
  tabBarLabel: 'Buytickets',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}

const SignUpstack = createStackNavigator({
  Info: SignUp,
});

SignUpstack.navigationOptions = {
  tabBarLabel: 'Buy ticket',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
}


export default createBottomTabNavigator({
  HomeStack,
  // Qr,
  // HomeStack,
  // LinksStack,
  images,
  DskStack,
  // SettingsStack,
  DskStack1,
  Trivia,
  // tkt,
  // newoffers,
  // startingStack,
  SignUpstack
});
