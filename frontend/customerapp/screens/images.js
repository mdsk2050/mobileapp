import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button,
  Share,
} from 'react-native';
import * as axios from 'axios';

export default class Galleries extends Component {

  constructor(props) {
    super(props);
    this.state = {
      gallerydata: [ ]
    };
  }

  static navigationOptions = {
    title: 'Theme Park Gallery',
  };

  componentWillMount() {
    axios.get("http://192.168.1.33:3000/api/Galleries/gallery").then((response) => {
        console.log("PETTAAAA MOVIE",JSON.stringify(response.data));
        this.setState({gallerydata: []});
              var obj = Object.assign(this.state, {gallerydata: JSON.parse(JSON.stringify(response.data))});
              this.setState(obj);
              console.log(this.state.gallerydata)
    })
          }

  // addProductToCart = () => {
  //   Alert.alert('Success', 'The product has been added to your cart')
  // }
  // onPress={() => this.addProductToCart()}

  onShare = (item) => {
    const content = { 
      message: '   *** Themepark Images *** To View This image click on the link '+'  ' +item+ '   '
      // ,
      // title: 'tile share',
      // url: 'https://res.cloudinary.com/simplotel/image/upload/w_5000,h_3333/x_0,y_0,w_5000,h_2812,r_0,c_crop,q_60,fl_progressive/w_455,f_auto,c_fit/wonderla-amusement-parks-resort/_60A0879_stc1on',
    };
    const option = { dialogTitle: 'title title title' };
    Share.share(content, option);
  }


  render() {
    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={this.state.gallerydata}
          horizontal={false}
          numColumns={2}
          keyExtractor= {(item) => {
            return item.id;
          }}
          ItemSeparatorComponent={() => {
            return (
              <View style={styles.separator}/>
            )
          }}
          renderItem={(post) => {
            const item = post.item;
            return (
              <View style={styles.card}>
                <Image style={styles.cardImage} source={{uri:item.image}}/>
                <View style={styles.cardFooter}>
                  <View style={styles.socialBarContainer}>
                    <View style={styles.socialBarSection}>
                      <TouchableOpacity style={styles.socialBarButton} onPress={() => this.onShare(item.image)} >
                      {/* https://png.icons8.com/flat_round/50/000000/share.png */}
                        <Image style={styles.icon} source={{uri: 'http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c543.png'}}/>
                        <Text style={[styles.socialBarLabel, styles.share]} >Share</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.socialBarSection}>
                      <TouchableOpacity style={styles.socialBarButton}>
                        <Image style={styles.icon} source={{uri: 'https://png.icons8.com/color/50/000000/hearts.png'}}/>
                        <Text style={styles.socialBarLabel}>{item.likes}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )
          }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
    backgroundColor:"#eee"
  },
  list: {
    paddingHorizontal: 5,
    backgroundColor:"#E6E6E6",
  },
  listContainer:{
    alignItems:'center'
  },
  separator: {
    marginTop: 10,
  },
  /******** card **************/
  card:{
    marginVertical: 8,
    flexBasis: '47%',
    marginHorizontal: 5,
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage:{
    flex: 1,
    height: 150,
    width: null,
  },
  /******** card components **************/
  share:{
    color: "#25b7d3",
  },
  icon: {
    width:25,
    height:25,
  },
  /******** social bar ******************/
  socialBarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1
  },
  socialBarSection: {
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  socialBarlabel: {
    marginLeft: 8,
    alignSelf: 'flex-end',
    justifyContent: 'center',
  },
  socialBarButton:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }
});  