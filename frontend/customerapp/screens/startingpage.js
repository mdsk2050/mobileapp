import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  FlatList,
  Modal,
  TouchableOpacity
} from 'react-native';
import {
  Notifications,
} from 'expo';
import axios from 'axios';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'Male', value: 0 },
  {label: 'Female', value: 1 }
];
export default class StartingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      gender: 0,
      tickettype:'',
      nameError:false,
      card:true,
      ticketsdata: [ ],
      notification: {}
    },
    this.state1 = {
      data1: [
        {id:1, title: "Adult Regular ",                  time:"Rs. 1000/- +18% GST",    image:"https://thumbs.dreamstime.com/b/ticket-template-modern-trendy-design-vector-admit-ticket-event-date-raw-seat-cinema-movie-live-music-concert-116106738.jpg"},
        {id:2, title: "Adult Fast Track",             time:" : Rs. 1500/- +18% GST", image:"https://t3.ftcdn.net/jpg/01/95/99/88/500_F_195998865_QrfkVeoCBp9ZHtpOJ2cxKF3lyF8VDGJU.jpg"} ,
        {id:3, title: "Child* Regular",            time:"Price Rs. 650 + 18% GST",    image:"https://mobiledakho.com/i/tm/3563/concert-ticket-template-free-download_tm38_3563_m.jpg"}, 
        {id:4, title: "Child* Fast Track",         time:"Price Rs. 1000 + 18% GST",  image:"https://mobiledakho.com/i/tm/3563/fake-plane-ticket-template-music-airport_tm6_3563_m.jpg"}, 
      ]
    };
  }

  static navigationOptions = {
    title: 'Buytickets',
  };

  componentDidMount() {
    this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }
  _handleNotification = (notification) => {
    this.setState({notification: notification});
    console.log('hello  ',JSON.stringify(this.state.notification.data.generateoffers))
    const {navigate} = this.props.navigation;
    navigate('LatestOffers',this.state.notification.data.generateoffers)
  };

  componentWillMount() {
    axios.get("http://192.168.1.33:3000/api/Tickets/parkTickets").then((response) => {
        console.log("PETTAAAA MOVIE",JSON.stringify(response.data));
        this.setState({ticketsdata: []});
              var obj = Object.assign(this.state, {ticketsdata: JSON.parse(JSON.stringify(response.data))});
              this.setState(obj);
              console.log(this.state.ticketsdata)
    })
          }


  async onClickListener1() {
    await  console.log(JSON.stringify(this.state))
     if(this.state.name.length  === 0){
       await this.setState({nameError:true})
     }else {
      await this.setState({nameError:false})
      await this.setState({card:false})
     } 
    }


  async onClickListener(item) {
    // Alert.alert("data"+JSON.stringify(item));
  await  console.log(JSON.stringify(this.state))
  await this.setState({tickettype:item})
  const {navigate} = this.props.navigation;
  //  if(this.state.name.length  === 0){
  //    await this.setState({nameError:true})
  //  }else {
  //   await this.setState({nameError:false})
    await axios.request({
      method:'post',
      url: 'http://192.168.1.33:3000/api/Customerdata/saveCustomerData',
      data: this.state,
    }).then(res =>{
      console.log('success')
      navigate('Qr',res)
    }).catch(err =>{
      console.log('error')
    })
  //  } 
  }
  static navigationOptions = {
    title:'BuyTicket',
  };

  render() {
    return (
      <View style={styles.tktcontainer}>
        {
          this.state.card ?( <View style={styles.container}>
      
      {/* https://media0.giphy.com/media/26FmRffyazBJwCWFW/200.webp?cid=3640f6095c481697476d412f6364e448 */}
            <Image style={styles.logo} source={{uri: 'https://cdn.dribbble.com/users/1878767/screenshots/3993661/gifs_2.gif'}}/>
    
            <View style={styles.inputContainer}>
              <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/user/ultraviolet/50/3498db'}}/>
              <TextInput style={styles.inputs}
                  placeholder="Name"
                  underlineColorAndroid='transparent'
                  onChangeText={async (name)  => {
                    
                    await this.setState({name})
                    if(this.state.name.length  === 0){
                      await this.setState({nameError:true})
                    }else {
                     await this.setState({nameError:false})
                    } 
                
                }}/>
            </View>
            <View style={{alignItems:'center'}}>
                  { this.state.nameError?<View><Text style={{color:'red'}}>Please Enter Your Name</Text></View>:<View><Text></Text></View>}
                  </View>
    
            <View style={styles.inputContainerGender}>
              <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/ultraviolet/40/000000/gender.png'}}/>
              <Text style={{fontWeight:'bold'}}>Gender</Text>
                  <RadioForm
                    radio_props={radio_props}
                    initial={0}
                    onPress={(gender) => {this.setState({gender:gender})}}
                  />
            </View>
    
            <TouchableHighlight style={[styles.buttonContainer, styles.sendButton]} onPress={() => this.onClickListener1()}>
              <Text style={styles.buttonText}>Next</Text>
            </TouchableHighlight>
          </View>):
          (
            <View style={styles.container1}>
        <FlatList style={styles.list}
          data={this.state.ticketsdata}
          keyExtractor= {(item) => {
            return item.id.toString();
          }}
          ItemSeparatorComponent={() => {
            return (
              <View style={styles.separator}/>
            )
          }}
          renderItem={(post) => {
            const item = post.item;
            return (
              <View style={styles.card} >
                <Image style={styles.cardImage} source={{uri:item.image}}/>
                <View style={styles.cardHeader}>
                  <View>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.description}>Price:{item.price}</Text>
                   
                  </View>
                </View>
                <View style={styles.cardFooter}>
                  <View style={styles.socialBarContainer}>
                    <View style={styles.socialBarSection}>
                      <TouchableOpacity style={styles.socialBarButton} onPress={() => this.onClickListener(item.title)}>
                        <Image style={styles.icon} source={{uri: 'https://png.icons8.com/material/96/2ecc71/visible.png'}}/>
                        <Text style={styles.socialBarLabel}>100</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.socialBarSection}>
                      <TouchableOpacity style={styles.socialBarButton}>
                        <Image style={styles.icon1} source={{uri: 'http://www.morettadifano.it/en/img/buy-here.gif'}}/>
                        <Text style={styles.socialBarLabel} onPress={() => this.onClickListener(item.title)}>Buy Now</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )
          }}/>
      </View>
          )
        }
     
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tktcontainer:{
    flex:1,
    // backgroundColor:'f4bee8'
    // marginTop:20,
  },
  list: {
    backgroundColor:"#E6E6E6",
  },
  separator: {
    marginTop: 1,
  },
  card:{
    margin: 0,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: "#DCDCDC",
    backgroundColor: "#DCDCDC",
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  logo:{
    width:300,
    height:200,
    justifyContent: 'center',
    marginBottom:20,
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputContainerGender: {
      width:250,
      marginBottom:20,
      paddingTop:10,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:100,
    borderRadius:30,
  },
  sendButton: {
    backgroundColor: "#FF4500",
  },
  buttonText: {
    color: 'white',
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
    //overlay efect
    flex: 1,
    height: 200,
    width: null,
    position: 'absolute',
    zIndex: 100,
    left: 0,
    right: 0,
    backgroundColor: 'transparent'
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    paddingBottom: 0,
    paddingVertical: 7.5,
    paddingHorizontal: 0
  },
  card:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    marginVertical: 8,
    backgroundColor:"white"
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
    backgroundColor:"#EEEEEE",
  },
  cardImage:{
    flex: 1,
    height: 150,
    width: null,
  },
  /******** card components **************/
  title:{
    fontSize:18,
    flex:1,
  }, 
  description:{
    fontSize:15,
    color:"#888",
    flex:1,
    marginTop:5,
    marginBottom:5,
  },
  time:{
    fontSize:13,
    color: "#808080",
    marginTop: 5
  },
  icon: {
    width:25,
    height:25,
  },
  icon1: {
    width:50,
    height:50,
  },
  iconData:{
    width:15,
    height:15,
    marginTop:5,
    marginRight:5
  },
  timeContainer:{
    flexDirection:'row'
  },
  /******** social bar ******************/
  socialBarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1
  },
  socialBarSection: {
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  socialBarlabel: {
    marginLeft: 8,
    alignSelf: 'flex-end',
    justifyContent: 'center',
  },
  socialBarButton:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container1:{
    flex:1,
    marginTop:20,
  },
}); 