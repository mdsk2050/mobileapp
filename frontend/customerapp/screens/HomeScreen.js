            
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image,
  Alert,
  ScrollView,
} from 'react-native';

export default class HomeScreen extends Component {

  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    header: null,
  };

  // onClickListener = (viewId) => {
  //   Alert.alert("Alert", "Button pressed ");
  // }

  render() {
    const {navigate} = this.props.navigation;
    return (
    //   <ScrollView style={styles.scrollContainer}>
        <View style={styles.container}>
          <Image style={styles.logo} source={{uri: 'https://us.123rf.com/450wm/grgroup/grgroup1408/grgroup140800569/30457820-stock-vector-theme-park-design-over-landscape-background-vector-illustration.jpg?ver=6'}}/>
          <Text style={styles.companyName}>Themepark.com</Text>
          <Text style={styles.slogan}>Welcome to ThemePark!</Text>
          <View style={styles.descriptionContent}>
            <Text style={styles.description}>
It's a place to celebrate the smaller moments and share big wonders
            </Text>
          </View>
          <TouchableHighlight style={[styles.buttonContainer, styles.sendButton]}  onPress={() => navigate('Buytickets')}>
          {/* onPress={() => this.onClickListener('login')} */}
            <Text style={styles.buttonText}>Lets Go</Text>
            {/* https://1.bp.blogspot.com/-boY_sxIC3Mw/WkZ6oyzuiWI/AAAAAAAAGqs/GcH36b5rptIb8hrM3O4mS5Pz4ah4yTevgCLcBGAs/s1600/join+now.gif */}
          </TouchableHighlight>
            <Image style={styles.logo1} source={{uri: 'https://media.istockphoto.com/vectors/lets-go-on-an-adventure-vector-id888026670?k=6&m=888026670&s=612x612&w=0&h=CGQ6MKl2a43LlgzZxFYaVdlqdOD1hZVeQEsyDvpi1Yg='}}/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  // scrollContainer:{
  //   flex: 1,
  // },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'black',
    // #1c1b1b
  },
  logo:{
    width:500,
    height:200,
    justifyContent: 'center',
    marginBottom:10,
    marginTop:30,
  },
  companyName: {
    fontSize:32,
    fontWeight: '600',
    color: '#FFFFFF',
  },
  slogan:{
    fontSize:20,
    fontWeight: '600',
    color: '#228B22',
  },
  descriptionContent:{
    padding:30
  },
  description:{
    fontSize:18,
    textAlign:'center',
    marginTop:10,
    color: '#FFFFFF',
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:100,
    borderRadius:30,
    
  },
  sendButton: {
    backgroundColor: "#404142",
  },
  buttonText: {
    fontSize:18,
    fontWeight: '600',
    color: '#228B22',
    marginTop:10,
  },
  logo1:{
    width:300,
    height:200,
    justifyContent: 'center',
  },
}); 