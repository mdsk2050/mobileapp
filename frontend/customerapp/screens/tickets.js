import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button
} from 'react-native';

export default class Posts extends Component {

    
    

  constructor(props) {
    super(props);
    this.state = {
      data: [
        {id:1, title: "Adult Regular ",                  time:"Rs. 1000/- +18% GST",    image:"https://thumbs.dreamstime.com/b/ticket-template-modern-trendy-design-vector-admit-ticket-event-date-raw-seat-cinema-movie-live-music-concert-116106738.jpg"},
        {id:2, title: "Adult Fast Track",             time:" : Rs. 1500/- +18% GST", image:"https://t3.ftcdn.net/jpg/01/95/99/88/500_F_195998865_QrfkVeoCBp9ZHtpOJ2cxKF3lyF8VDGJU.jpg"} ,
        {id:3, title: "Child* Regular",            time:"Price Rs. 650 + 18% GST",    image:"https://mobiledakho.com/i/tm/3563/concert-ticket-template-free-download_tm38_3563_m.jpg"}, 
        {id:4, title: "Child* Fast Track",         time:"Price Rs. 1000 + 18% GST",  image:"https://mobiledakho.com/i/tm/3563/fake-plane-ticket-template-music-airport_tm6_3563_m.jpg"}, 
      ]
    };
  }

  render() {
    return (
      <View style={styles.tktcontainer}>
      {/* <Image style={styles.cardImage1} source={{uri: 'http://dreamlandamusements.com/wp-content/uploads/2018/02/BuyTickets.gif'}}/> */}
        <FlatList style={styles.list}
          data={this.state.data}
          keyExtractor= {(item) => {
            return item.id;
          }}
          ItemSeparatorComponent={() => {
            return (
              <View style={styles.separator}/>
            )
          }}
          renderItem={(post) => {
            const item = post.item;
            return (
                <View style={styles.card}>

                  <Image style={styles.cardImage} source={{uri:item.image}}/>
                  {/* <View style={styles.cardContent}>
                    <View>
                      <Text style={styles.title}>{item.title}</Text>
                      <Text style={styles.time}>{item.time}</Text>
                    </View>
                </View> */}
                </View> 
                
            )
          }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
 tktcontainer:{
    flex:1,
    marginTop:20,
  },
  list: {
    backgroundColor:"#E6E6E6",
  },
  separator: {
    marginTop: 1,
  },
  /******** card **************/
  card:{
    margin: 0,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: "#DCDCDC",
    backgroundColor: "#DCDCDC",
  },
  card1:{
    margin: 0,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: "#DCDCDC",
    backgroundColor: "#DCDCDC",
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
    //overlay efect
    flex: 1,
    height: 200,
    width: null,
    position: 'absolute',
    zIndex: 100,
    left: 0,
    right: 0,
    backgroundColor: 'transparent'
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    paddingBottom: 0,
    paddingVertical: 7.5,
    paddingHorizontal: 0
  },
  cardImage:{
    flex: 1,
    height: 150,
    width: null,
  },
  cardImage1:{
    width:420,
    height:200,
    // justifyContent: 'center',
    
  },
  logo:{
    width:300,
    height:200,
    justifyContent: 'center',
    marginBottom:20,
  },
  /******** card components **************/
  title:{
    fontSize:22,
    color: "#0699e8",
    marginTop: 10,
    fontWeight:'bold'
  },
  time:{
    fontSize:13,
    color: "#ffffff",
    marginTop: 5
  },
  icon: {
    width:25,
    height:25,
  },
  /******** social bar ******************/
  socialBarContainer: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    flex: 1
  },
  socialBarSection: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 1,
  },
  socialBarlabel: {
    marginLeft: 8,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    color: "#ffffff",
  },
  socialBarButton:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }
}); 