import React, { Component } from 'react';
import {StyleSheet,Text,View,TouchableOpacity,Image,FlatList} from 'react-native';
import axios from 'axios'
import PureChart from 'react-native-pure-chart';
export default class CustomerChart extends Component {
    constructor(props){
      super(props)
      this.state = {
        data:[],
      }
    }

    static navigationOptions = {
      title: 'Gender Visuavalization',
    };
    
    componentDidMount() {
      console.log('hi')
      axios.get('http://192.168.1.33:3000/api/Customerdata/getCustomerData').then((res)=>{
        console.log(JSON.stringify(res.data))
        this.setState({data: []});
        var state = Object.assign(this.state, {data: res.data});
        this.setState(state);
      })
    }
  

  render() {
    return (
        <View style={styles.container}>
        <PureChart data={this.state.data} type='bar' />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: "#FFFFFF",
    alignItems: 'flex-start'
  },
});  
