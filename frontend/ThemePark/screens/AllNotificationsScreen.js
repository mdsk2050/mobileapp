import React, { Component } from 'react';
import {StyleSheet,Text,View,TouchableOpacity,Image,FlatList} from 'react-native';
import {
  Notifications,
} from 'expo';
import registerForPushNotificationsAsync  from '../moduleFunctions/registerForPushNotificationsAsync'
import axios from 'axios'
export default class AllNotificationsScreen extends Component {
    constructor(props){
      super(props)
      this.state = {
        data:[],
        
        // notification: {}
      }
    }
    static navigationOptions = {
      title: 'Tickets Puchase History',
    };
    componentDidMount() {
      console.log('hi')
      axios.get('http://192.168.1.33:3000/api/Customerdata').then((res)=>{
        console.log(JSON.stringify(res.data))
        this.setState({data: []});
        var state = Object.assign(this.state, {data: res.data});
        this.setState(state);
      })
      // registerForPushNotificationsAsync();
      // this._notificationSubscription = Notifications.addListener(this._handleNotification);
    }
    // _handleNotification = (notification) => {
    //   this.setState({notification: notification});
    //   console.log(JSON.stringify(this.state.notification.data))
    //   const {navigate} = this.props.navigation;
    //   navigate('Qr',data)
    // };
  

  render() {
    return (
      <FlatList
      style={styles.root}
      data={this.state.data}
      extraData={this.state}
      ItemSeparatorComponent={() => {
        return (
          <View style={styles.separator}/>
        )
      }}
      keyExtractor={(item)=>{
        return item.id.toString();
      }}
      renderItem={(item) => {
        const Notification = item.item;

        let mainContentStyle;
        mainContentStyle = styles.mainContent;
        return(
          <View style={styles.container}>
            <Image source={{uri:Notification.image}} style={styles.avatar}/>
            <View style={styles.content}>
              <View style={mainContentStyle}>
                <View style={styles.text}>
                  <Text style={styles.name}>Name: {Notification.name}</Text>
                </View>
                <View style={styles.text}>
                <Text>Gender: {Notification.gender === '0'?'Male':'Female'}</Text>
                </View>
                <View style={styles.text}>
                <Text>Ticket Number: {Notification.ticketnumber}</Text>
                </View>
                <Text style={styles.timeAgo}>
                  Time: {Notification.time}
                </Text>
              </View>
            </View>
          </View>
        );
      }}/>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#FFFFFF"
  },
  container: {
    padding: 16,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: "#FFFFFF",
    alignItems: 'flex-start'
  },
  avatar: {
    width:50,
    height:50,
    borderRadius:25,
  },
  text: {
    marginBottom: 5,
    flexDirection: 'row',
    flexWrap:'wrap'
  },
  content: {
    flex: 1,
    marginLeft: 16,
    marginRight: 0
  },
  mainContent: {
    marginRight: 60
  },
  img: {
    height: 50,
    width: 50,
    margin: 0
  },
  separator: {
    height: 1,
    backgroundColor: "#CCCCCC"
  },
  timeAgo:{
    fontSize:12,
    color:"#696969"
  },
  name:{
    fontSize:16,
    color:"#1E90FF"
  }
});  
