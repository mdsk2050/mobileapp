import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  TouchableHighlight
} from 'react-native';
import axios from 'axios';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: '50%', value: 0 },
  {label: '25%', value: 1 }
];

export default class OffersScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            time: 0,
          nameError:false,
        }
      }

    static navigationOptions = {
        title: 'ThemePark Admin Offers Screen',
      };

      async onClickListener() {
      await  console.log(JSON.stringify(this.state))
    //   const {navigate} = this.props.navigation;
       if(this.state.title.length  === 0){
         await this.setState({nameError:true})
       }else {
        await this.setState({nameError:false})
        await axios.request({
          method:'post',
          url: 'http://192.168.1.33:3000/api/Offers/GenearateOffers',
          data: this.state,
        }).then(res =>{
          console.log('success')
        //   navigate('Qr',res)
        }).catch(err =>{
          console.log('error')
        })
       } 
      }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.header}>
              <Text style={styles.headerTitle}>
                Generating Offers
              </Text>
          </View>

          <View style={styles.postContent}>
              <Text style={styles.postTitle}>
                ThemePark Admin 
              </Text>

              <Text style={styles.postDescription}>
              Business processes are how things are done within a business. ... Successful businesses understand the need to continuously improve their business processes: to become more efficient and productive, and to respond to market changes faster while providing better service to customers. Information technology. 
              </Text>

              <Text style={styles.tags}>
              customers are the most important assets 
              </Text>

              <Text style={styles.date}>
                2019-JAN-25
            </Text>

              <View style={styles.profile}>
                {/* <Image style={styles.avatar}
                  source={{uri: 'https://2.bp.blogspot.com/-aydBOz1QLSE/WjXzRU3waHI/AAAAAAAAAEk/XkCEy7gI4qghZK3UgbKZWUjPV0SybmoFACLcBGAs/s320/special-offer.gif'}}/> */}

                {/* <Text style={styles.name}>
                    Generate Offers
                </Text> */}
              </View>
              <View></View>
              <View style={styles.inputContainer}>
              <Image style={styles.inputIcon} source={{uri: 'https://2.bp.blogspot.com/-aydBOz1QLSE/WjXzRU3waHI/AAAAAAAAAEk/XkCEy7gI4qghZK3UgbKZWUjPV0SybmoFACLcBGAs/s320/special-offer.gif'}}/>
              <TextInput style={styles.inputs}
                  placeholder="Generate Offer"
                  underlineColorAndroid='transparent'
                  onChangeText={async (title)  => {
                    
                    await this.setState({title})
                    if(this.state.title.length  === 0){
                      await this.setState({nameError:true})
                    }else {
                     await this.setState({nameError:false})
                    } 
                
                }}/>
            </View>
            <View style={{alignItems:'center'}}>
                  { this.state.nameError?<View><Text style={{color:'red'}}>Please Enter Your Title</Text></View>:<View><Text></Text></View>}
                  </View>
    
            {/* <View style={styles.inputContainerGender}>
              <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/ultraviolet/40/000000/gender.png'}}/>
              <Text style={{fontWeight:'bold'}}>Gender</Text>
                  <RadioForm
                    radio_props={radio_props}
                    initial={0}
                    onPress={(gender) => {this.setState({gender:gender})}}
                  />
            </View> */}
          </View>
              <TouchableOpacity style={styles.shareButton} onPress={() => this.onClickListener()}>
                <Text style={styles.shareButtonText} >Send Offers</Text>  
              </TouchableOpacity> 
          </View>
        
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
  },
  header:{
    padding:15,
    alignItems: 'center',
    backgroundColor: "#00BFFF",
  },
  headerTitle:{
    fontSize:20,
    color:"#FFFFFF",
    marginTop:10,
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  postContent: {
    flex: 1,
    padding:30,
  },
  postTitle:{
    fontSize:26,
    fontWeight:'600',
  },
  postDescription:{
    fontSize:16,
    marginTop:10,
  },
  tags:{
    color: '#00BFFF',
    marginTop:10,
  },
  date:{
    color: '#696969',
    marginTop:10,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 35,
    borderWidth: 4,
    borderColor: "#00BFFF",
  },
  profile:{
    flexDirection: 'row',
    marginTop:20
  },
  name:{
    fontSize:22,
    color:"#00BFFF",
    fontWeight:'600',
    alignSelf:'center',
    marginLeft:10
  }, 
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  /**
   * latest added CSS
   */
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:350,
    height:45,
    marginBottom:20,
    flexDirection: 'row',
    alignItems:'center'
},
inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  inputs:{
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
},
inputContainerGender: {
    width:250,
    marginBottom:20,
    paddingTop:10,
    flexDirection: 'row',
    alignItems:'center'
},
});