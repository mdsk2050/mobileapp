import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import ThemeParkUpdates from '../screens/ThemeParkUpdates';
import AllNotificationsScreen from '../screens/AllNotificationsScreen';
import QRcode from '../screens/QRcode'
import CustomerChart from '../screens/CustomerCharts'
import OffersScreen from '../screens/offers';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Qr: QRcode
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-home${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

const ThemPark = createStackNavigator({
  Links: ThemeParkUpdates,
});

ThemPark.navigationOptions = {
  tabBarLabel: 'Updates',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-information-circle' : 'md-information-circle'}
    />
  ),
};

const TicketsNotifications = createStackNavigator({
  Settings: AllNotificationsScreen,
});

TicketsNotifications.navigationOptions = {
  tabBarLabel: 'Tickets',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};
const Chart = createStackNavigator({
  chart: CustomerChart,
});

Chart.navigationOptions = {
  tabBarLabel: 'Chart',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'}
    />
  ),
};

const Offers = createStackNavigator({
  offers: OffersScreen,
});

Offers.navigationOptions = {
  tabBarLabel: 'Offers',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'}
    />
  ),
};

export default createBottomTabNavigator({
  HomeStack,
  ThemPark,
  TicketsNotifications,
  // Qr,
  Chart,
  Offers
});
