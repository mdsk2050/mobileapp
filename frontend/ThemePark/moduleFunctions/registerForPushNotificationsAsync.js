import { Permissions, Notifications } from 'expo';
import axios from 'axios'

async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  if (finalStatus !== 'granted') {
    return;
  }

  let token = await Notifications.getExpoPushTokenAsync();

  let Obj = {
    token:token
  }
  console.log("tokennnnnnnnnnnn"+JSON.stringify(Obj))
    await axios.request({
    method:'post',
    url: 'http://192.168.1.33:3000/api/Notifications/pushNotificationToken',
    data: Obj,
  }).then(res =>{
    console.log('success')
  })
  // .catch(err =>{
  //   console.log('error',err)
  // })
}
export default registerForPushNotificationsAsync